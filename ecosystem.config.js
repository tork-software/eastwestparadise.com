module.exports = {
  apps: [
    {
      name: 'East West Paradise',
      exec_mode: 'cluster',
      instances: 'max',
      script: './.output/server/index.mjs'
    }
  ]
}
