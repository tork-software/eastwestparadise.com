import { defineNuxtConfig } from 'nuxt3'

// https://v3.nuxtjs.org/docs/directory-structure/nuxt.config
export default defineNuxtConfig({

      meta: {
        meta: [
          // <meta name="viewport" content="width=device-width, initial-scale=1">
          { name: 'viewport', content: 'width=device-width, initial-scale=1' }
        ],
        link: [
          { rel: 'icon', type:'image/png', href: '/assets/images/favicon.png' },
          { rel: 'stylesheet', href: '/assets/css/bootstrap.min.css' },
          { rel: 'stylesheet', href: '/assets/css/responsive.css' },
          { rel: 'stylesheet', href: '/assets/css/style.css' }
        ],
        // please note that this is an area that is likely to change
        style: [
          // <style type="text/css">:root { color: red }</style>
          { children: 'html { }', type: 'text/css' }
        ]
       }
     
       

})
